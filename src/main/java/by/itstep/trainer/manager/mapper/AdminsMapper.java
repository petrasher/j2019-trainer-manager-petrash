package by.itstep.trainer.manager.mapper;
import by.itstep.trainer.manager.dto.adminsDto.AdminsPreviewDto;
import by.itstep.trainer.manager.dto.adminsDto.AdminsCreateDto;
import by.itstep.trainer.manager.dto.adminsDto.AdminsUpdateDto;
import by.itstep.trainer.manager.dto.adminsDto.AdminsFullDto;
import by.itstep.trainer.manager.entity.Admins;

import java.util.ArrayList;
import java.util.List;

public class AdminsMapper {

    public List<AdminsPreviewDto> mapToDtoList(List<Admins> entities){
        List<AdminsPreviewDto> dtos = new ArrayList<>();

        for(Admins entity : entities){
            AdminsPreviewDto dto = new AdminsPreviewDto();

            dto.setId(entity.getId());
            dto.setName(entity.getName());
            dto.setLastName(entity.getLastName());
            dto.setRole(entity.getRole());

            dtos.add(dto);
        }

        return dtos;

    }

    public Admins mapToEntity(AdminsCreateDto createDto){
        Admins admins = new Admins();
        admins.setName(createDto.getName());
        admins.setLastName(createDto.getLastName());
        admins.setEmail(createDto.getEmail());
        admins.setPassword(createDto.getPassword());

        return admins;
    }

    public Admins mapToEntity (AdminsUpdateDto updateDto){
        Admins admins = new Admins();
        admins.setId(updateDto.getId());
        admins.setName(updateDto.getName());
        admins.setLastName(updateDto.getLastName());
        admins.setEmail(updateDto.getEmail());
        admins.setPassword(updateDto.getPassword());

        return admins;

    }

    public AdminsFullDto mapToDto(Admins entity){
        AdminsFullDto fullDto = new AdminsFullDto();
        fullDto.setId(entity.getId());
        fullDto.setName(entity.getName());
        fullDto.setLastName(entity.getLastName());
        fullDto.setRole(entity.getRole());
        fullDto.setEmail(entity.getEmail());

        return fullDto;
    }


}
