package by.itstep.trainer.manager.mapper;

import by.itstep.trainer.manager.dto.commentsDto.CommentsCreateDto;
import by.itstep.trainer.manager.dto.commentsDto.CommentsFullDto;
import by.itstep.trainer.manager.dto.commentsDto.CommentsPreviewDto;
import by.itstep.trainer.manager.dto.commentsDto.CommentsUpdateDto;
import by.itstep.trainer.manager.entity.Comments;
import by.itstep.trainer.manager.entity.Trainer;

import java.util.ArrayList;
import java.util.List;

public class CommentsMapper {

    public List<CommentsPreviewDto> mapToDtoList(List<Comments> entities) {
        List<CommentsPreviewDto> dtos = new ArrayList<>();

        for (Comments entity : entities) {
            CommentsPreviewDto dto = new CommentsPreviewDto();

            dto.setId(entity.getId());
            dto.setTrainer(entity.getTrainer());
            dto.setComment(entity.getComment());
            dto.setName(entity.getName());
            dto.setLastName(entity.getLastName());
            dto.setEmail(entity.getEmail());
            dto.setRating(entity.getRating());
            dto.setPublished(entity.isPublished());

            dtos.add(dto);
        }

        return dtos;
    }

    public Comments mapToEntity(CommentsCreateDto createDto, Trainer trainer) {
        Comments comments = new Comments();
        comments.setTrainer(createDto.getTrainer());
        comments.setName(createDto.getName());
        comments.setLastName(createDto.getLastName());
        comments.setEmail(createDto.getEmail());
        comments.setRating(createDto.getRating());

        return comments;

    }

    public Comments mapToEntity(CommentsUpdateDto updateDto) {
        Comments comments = new Comments();
        comments.setId(updateDto.getId());
        comments.setComment(updateDto.getComment());
        comments.setRating(updateDto.getRating());

        return comments;
    }

    public CommentsFullDto mapToDto(Comments entity) {
        CommentsFullDto fullDto = new CommentsFullDto();
        fullDto.setId(entity.getId());
        fullDto.setTrainer(entity.getTrainer());
        fullDto.setName(entity.getName());
        fullDto.setLastName(entity.getLastName());
        fullDto.setComment(entity.getComment());
        fullDto.setEmail(entity.getEmail());
        fullDto.setRating(entity.getRating());
        fullDto.setPublished(entity.isPublished());

        return fullDto;
    }
}
