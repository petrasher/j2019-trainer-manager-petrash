package by.itstep.trainer.manager.mapper;

import by.itstep.trainer.manager.dto.usersRecordingDto.UsersRecordingCreateDto;
import by.itstep.trainer.manager.dto.usersRecordingDto.UsersRecordingFullDto;
import by.itstep.trainer.manager.dto.usersRecordingDto.UsersRecordingPreviewDto;
import by.itstep.trainer.manager.dto.usersRecordingDto.UsersRecordingUpdateDto;
import by.itstep.trainer.manager.entity.Trainer;
import by.itstep.trainer.manager.entity.UsersRecording;

import java.util.ArrayList;
import java.util.List;

public class UsersRecordingMapper {


    public List<UsersRecordingPreviewDto> mapToDtoList(List<UsersRecording> entities) {
        List<UsersRecordingPreviewDto> dtos = new ArrayList<>();

        for (UsersRecording entity : entities) {
            UsersRecordingPreviewDto dto = new UsersRecordingPreviewDto();

            dto.setId(entity.getId());
            dto.setName(entity.getName());
            dto.setLastName(entity.getLastName());
            dto.setRecordingTime(entity.getRecordingTime());
            dto.setTrainer(entity.getTrainer());

            dtos.add(dto);
        }
        return dtos;
    }

    public UsersRecording mapToEntity(UsersRecordingCreateDto createDto, Trainer trainer) {
        UsersRecording recording = new UsersRecording();
        recording.setRecordingTime(createDto.getRecordingTime());
        recording.setComment(createDto.getComment());
        recording.setName(createDto.getName());
        recording.setLastName(createDto.getLastName());
        recording.setEmail(createDto.getEmail());
        recording.setPhone(createDto.getPhone());
        recording.setTrainer(trainer);

        return recording;
    }

    public UsersRecording mapToEntity(UsersRecordingUpdateDto updateDto) {
        UsersRecording recording = new UsersRecording();
        recording.setId(updateDto.getId());
        recording.setComment(updateDto.getComment());
        recording.setName(updateDto.getName());
        recording.setLastName(updateDto.getLastName());
        recording.setEmail(updateDto.getEmail());
        recording.setPhone(updateDto.getPhone());

        return recording;
    }

    public UsersRecordingFullDto mapToDto(UsersRecording entity) {
        UsersRecordingFullDto fullDto = new UsersRecordingFullDto();
        fullDto.setId(entity.getId());
        fullDto.setTrainer(entity.getTrainer());
        fullDto.setRecordingTime(entity.getRecordingTime());
        fullDto.setComment(entity.getComment());
        fullDto.setName(entity.getName());
        fullDto.setLastName(entity.getLastName());
        fullDto.setEmail(entity.getEmail());
        fullDto.setPhone(entity.getPhone());

        return fullDto;
    }

}
