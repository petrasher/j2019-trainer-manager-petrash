package by.itstep.trainer.manager.mapper;

import by.itstep.trainer.manager.dto.trainerDto.TrainerCreateDto;
import by.itstep.trainer.manager.dto.trainerDto.TrainerFullDto;
import by.itstep.trainer.manager.dto.trainerDto.TrainerPreviewDto;
import by.itstep.trainer.manager.dto.trainerDto.TrainerUpdateDto;
import by.itstep.trainer.manager.entity.Trainer;

import java.util.ArrayList;
import java.util.List;

public class TrainerMapper {

    public List<TrainerPreviewDto> mapToDtoList(List<Trainer> entities) {
        List<TrainerPreviewDto> dtos = new ArrayList<>();

        for (Trainer entity : entities) {
            TrainerPreviewDto dto = new TrainerPreviewDto();

            dto.setId(entity.getId());
            dto.setName(entity.getName());
            dto.setLastName(entity.getLastName());
            dto.setEmail(entity.getEmail());
            dto.setExperience(entity.getExperience());

            dtos.add(dto);
        }
        return dtos;
    }

    public Trainer mapToEntity(TrainerCreateDto createDto) {
        Trainer trainer = new Trainer();
        trainer.setName(createDto.getName());
        trainer.setLastName(createDto.getLastName());
        trainer.setExperience(createDto.getExperience());
        trainer.setProgress(createDto.getProgress());
        trainer.setPassword(createDto.getPassword());

        return trainer;
    }

    public Trainer mapToEntity(TrainerUpdateDto updateDto) {
        Trainer trainer = new Trainer();
        trainer.setId(updateDto.getId());
        trainer.setName(updateDto.getName());
        trainer.setLastName(updateDto.getLastName());
        trainer.setExperience(updateDto.getExperience());
        trainer.setProgress(updateDto.getProgress());
        trainer.setPassword(updateDto.getPassword());

        return trainer;

    }

    public TrainerFullDto mapToDto(Trainer entity) {
        TrainerFullDto fullDto = new TrainerFullDto();
        fullDto.setId(entity.getId());
        fullDto.setName(entity.getName());
        fullDto.setLastName(entity.getLastName());
        fullDto.setExperience(entity.getExperience());
        fullDto.setProgress(entity.getProgress());
        fullDto.setComments(entity.getComments());
        fullDto.setEmail(entity.getEmail());
        fullDto.setUsersRecordings(entity.getUsersRecordings());

        return fullDto;

    }

}
