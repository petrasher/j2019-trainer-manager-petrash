package by.itstep.trainer.manager.dto.adminsDto;


import by.itstep.trainer.manager.entity.enums.Role;
import lombok.Data;


@Data
public class AdminsFullDto {

    private Long id;

    private String name;

    private String lastName;

    private String email;

    private Role role;
}
