package by.itstep.trainer.manager.dto.usersRecordingDto;

import by.itstep.trainer.manager.entity.Trainer;
import lombok.Data;

import java.util.Date;

@Data
public class UsersRecordingUpdateDto {

    private Long id;

    private String name;

    private String comment;

    private String lastName;

    private String email;

    private String phone;
}
