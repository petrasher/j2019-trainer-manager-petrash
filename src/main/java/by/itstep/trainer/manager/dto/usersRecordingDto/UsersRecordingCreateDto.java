package by.itstep.trainer.manager.dto.usersRecordingDto;

import by.itstep.trainer.manager.entity.Trainer;
import lombok.Data;

import java.util.Date;

@Data
public class UsersRecordingCreateDto {

    private String name;

    private String comment;

    private String lastName;

    private String email;

    private String phone;

    private Date recordingTime;

    private Long trainerId;
}
