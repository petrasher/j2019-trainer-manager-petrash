package by.itstep.trainer.manager.dto.commentsDto;

import by.itstep.trainer.manager.entity.Trainer;
import lombok.Data;

@Data
public class CommentsUpdateDto {

    private Long id;

    private String comment;

    private int rating;

}
