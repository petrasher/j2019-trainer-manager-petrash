package by.itstep.trainer.manager.dto.adminsDto;

import lombok.Data;

@Data
public class AdminsUpdateDto {

    private Long id;

    private String name;

    private String lastName;

    private String email;

    private String password;
}
