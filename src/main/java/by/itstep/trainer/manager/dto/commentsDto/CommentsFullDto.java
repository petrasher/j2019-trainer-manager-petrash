package by.itstep.trainer.manager.dto.commentsDto;

import by.itstep.trainer.manager.entity.Trainer;
import lombok.Data;

@Data
public class CommentsFullDto {
    private Long id;

    private Trainer trainer;

    private String name;

    private String comment;

    private String LastName;

    private String email;

    private int rating;

    private boolean published;
}
