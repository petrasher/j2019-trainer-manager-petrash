package by.itstep.trainer.manager.dto.trainerDto;

import by.itstep.trainer.manager.entity.Comments;
import by.itstep.trainer.manager.entity.UsersRecording;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class TrainerCreateDto {

    private String name;

    private String lastName;

    private Date experience;

    private String email;

    private String progress;

    private String password;
}
