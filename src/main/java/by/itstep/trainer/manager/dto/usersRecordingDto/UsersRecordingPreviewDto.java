package by.itstep.trainer.manager.dto.usersRecordingDto;

import by.itstep.trainer.manager.entity.Trainer;
import lombok.Data;

import java.util.Date;

@Data
public class UsersRecordingPreviewDto {

    private Long id;

    private Trainer trainer;

    private String name;

    private String lastName;

    private Date recordingTime;
}
