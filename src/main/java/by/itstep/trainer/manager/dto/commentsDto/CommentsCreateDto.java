package by.itstep.trainer.manager.dto.commentsDto;

import by.itstep.trainer.manager.entity.Trainer;
import lombok.Data;

@Data
public class CommentsCreateDto {

    private Trainer trainer;

    private String name;

    private String lastName;

    private String email;

    private int rating;

    private Long trainerId;

}
