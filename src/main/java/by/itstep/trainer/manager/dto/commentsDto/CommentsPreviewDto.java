package by.itstep.trainer.manager.dto.commentsDto;

import by.itstep.trainer.manager.entity.Trainer;
import lombok.Data;

@Data
public class CommentsPreviewDto {

    private String comment;

    private String name;

    private String LastName;

    private String email;

    private int rating;

    private boolean published;
}
