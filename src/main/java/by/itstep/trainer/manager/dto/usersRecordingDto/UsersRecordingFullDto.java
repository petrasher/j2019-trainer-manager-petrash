package by.itstep.trainer.manager.dto.usersRecordingDto;

import by.itstep.trainer.manager.entity.Trainer;
import lombok.Data;

import java.util.Date;

@Data
public class UsersRecordingFullDto {

    private Long id;

    private Trainer trainer;

    private String name;

    private String comment;

    private String lastName;

    private String email;

    private String phone;

    private Date recordingTime;
}
