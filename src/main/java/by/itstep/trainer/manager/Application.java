package by.itstep.trainer.manager;

import by.itstep.trainer.manager.util.EntityManagerUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);

		EntityManagerUtil.getEntityManager();
	}

}
