package by.itstep.trainer.manager.entity;

import lombok.*;

import javax.persistence.*;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name ="comments")
public class Comments {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @ManyToOne
    @JoinColumn(name = "trainer_id")
    private Trainer trainer;

    @Column(name = "name")
    private String name;

    @Column(name = "last_name")
    private String LastName;

    @Column (name = "comment")
    private String comment;

    @Column(name = "email")
    private String email;

    @Column(name = "rating")
    private int rating;

    @Column(name = "published")
    private boolean published;
}
