package by.itstep.trainer.manager.service;

import by.itstep.trainer.manager.repository.AdminsRepository;
import by.itstep.trainer.manager.repository.AdminsRepositoryImpl;
import by.itstep.trainer.manager.dto.adminsDto.AdminsCreateDto;
import by.itstep.trainer.manager.dto.adminsDto.AdminsFullDto;
import by.itstep.trainer.manager.dto.adminsDto.AdminsPreviewDto;
import by.itstep.trainer.manager.dto.adminsDto.AdminsUpdateDto;
import by.itstep.trainer.manager.entity.Admins;
import by.itstep.trainer.manager.mapper.AdminsMapper;

import java.util.List;

public class AdminsServiceImpl implements AdminsService {

    public AdminsRepository adminsRepository = new AdminsRepositoryImpl();
    public AdminsMapper mapper = new AdminsMapper();

    @Override
    public List<AdminsPreviewDto> findAll() {
        List<Admins> found = adminsRepository.findAll();
        return mapper.mapToDtoList(found);
    }

    @Override
    public AdminsFullDto findById(Long id) {
        Admins found = adminsRepository.findById(id);
        return mapper.mapToDto(found);
    }

    @Override
    public AdminsFullDto create(AdminsCreateDto createDto) {
        Admins toSave = mapper.mapToEntity(createDto);

        Admins created = adminsRepository.create(toSave);
        return mapper.mapToDto(created);
    }

    @Override
    public AdminsFullDto update(AdminsUpdateDto updateDto) {
        Admins toUpdate = mapper.mapToEntity(updateDto);

        Admins existingEntity = adminsRepository.findById(updateDto.getId());
        toUpdate.setRole(existingEntity.getRole());
        Admins updated = adminsRepository.update(toUpdate);
        return mapper.mapToDto(updated);
    }

    @Override
    public void deleteById(Long id) {
        adminsRepository.deleteById(id);

    }

    @Override
    public void deleteAll() {

    }
}
