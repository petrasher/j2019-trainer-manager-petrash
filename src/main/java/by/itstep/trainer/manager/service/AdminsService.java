package by.itstep.trainer.manager.service;

import by.itstep.trainer.manager.dto.adminsDto.AdminsPreviewDto;
import by.itstep.trainer.manager.dto.adminsDto.AdminsCreateDto;
import by.itstep.trainer.manager.dto.adminsDto.AdminsUpdateDto;
import by.itstep.trainer.manager.dto.adminsDto.AdminsFullDto;

import java.util.List;

public interface AdminsService {

    List<AdminsPreviewDto> findAll();

    AdminsFullDto findById(Long id);

    AdminsFullDto create (AdminsCreateDto createDto);

    AdminsFullDto update (AdminsUpdateDto updateDto);

    void deleteById(Long id);

    void deleteAll();

}
