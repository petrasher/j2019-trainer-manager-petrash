package by.itstep.trainer.manager.service;

import by.itstep.trainer.manager.dto.commentsDto.CommentsCreateDto;
import by.itstep.trainer.manager.dto.commentsDto.CommentsFullDto;
import by.itstep.trainer.manager.dto.commentsDto.CommentsPreviewDto;
import by.itstep.trainer.manager.dto.commentsDto.CommentsUpdateDto;


import java.util.List;

public interface CommentsService {

    List<CommentsPreviewDto> findAll();

    CommentsFullDto findById(Long id);

    CommentsFullDto create (CommentsCreateDto createDto);

    CommentsFullDto update (CommentsUpdateDto updateDto);

    void deleteById(Long id);

    void deleteAll();
}
