package by.itstep.trainer.manager.service;

import by.itstep.trainer.manager.entity.Comments;
import by.itstep.trainer.manager.entity.Trainer;
import by.itstep.trainer.manager.mapper.CommentsMapper;
import by.itstep.trainer.manager.repository.CommentsRepository;
import by.itstep.trainer.manager.repository.CommentsRepositoryImpl;
import by.itstep.trainer.manager.repository.TrainerRepositoryImpl;
import by.itstep.trainer.manager.repository.TrainerRepository;
import by.itstep.trainer.manager.dto.commentsDto.CommentsCreateDto;
import by.itstep.trainer.manager.dto.commentsDto.CommentsFullDto;
import by.itstep.trainer.manager.dto.commentsDto.CommentsPreviewDto;
import by.itstep.trainer.manager.dto.commentsDto.CommentsUpdateDto;

import java.util.List;

public class CommentsServiceImpl implements CommentsService {

    private CommentsRepository commentsRepository = new CommentsRepositoryImpl();

    private TrainerRepository trainerRepository = new TrainerRepositoryImpl();

    private CommentsMapper mapper = new CommentsMapper();

    @Override
    public List<CommentsPreviewDto> findAll() {
        List<Comments> found = commentsRepository.findAll();
        return mapper.mapToDtoList(found);
    }

    @Override
    public CommentsFullDto findById(Long id) {
        Comments found = commentsRepository.findById(id);
        return mapper.mapToDto(found);
    }

    @Override
    public CommentsFullDto create(CommentsCreateDto createDto) {
        Trainer trainer = trainerRepository.findById(createDto.getTrainerId());
        Comments toSave = mapper.mapToEntity(createDto, trainer);

        Comments created = commentsRepository.create(toSave);

        return mapper.mapToDto(created);
    }

    @Override
    public CommentsFullDto update(CommentsUpdateDto updateDto) {
        Comments toUpdate = mapper.mapToEntity(updateDto);
        Comments existingEntity = commentsRepository.findById(updateDto.getId());

        toUpdate.setName(existingEntity.getName());
        toUpdate.setLastName(existingEntity.getLastName());
        toUpdate.setEmail(existingEntity.getEmail());
        toUpdate.setTrainer(existingEntity.getTrainer());

        Comments updated = commentsRepository.update(toUpdate);
        return mapper.mapToDto(updated);
    }

    @Override
    public void deleteById(Long id) {
        commentsRepository.deleteById(id);

    }

    @Override
    public void deleteAll() {

    }
}
