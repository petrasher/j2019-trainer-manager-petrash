package by.itstep.trainer.manager.service;

import by.itstep.trainer.manager.dto.usersRecordingDto.UsersRecordingCreateDto;
import by.itstep.trainer.manager.dto.usersRecordingDto.UsersRecordingFullDto;
import by.itstep.trainer.manager.dto.usersRecordingDto.UsersRecordingPreviewDto;
import by.itstep.trainer.manager.dto.usersRecordingDto.UsersRecordingUpdateDto;
import by.itstep.trainer.manager.entity.UsersRecording;

import java.util.List;

public interface UsersRecordingService {

    List<UsersRecordingPreviewDto> findAll();

    UsersRecordingFullDto findById(Long id);

    UsersRecordingFullDto create (UsersRecordingCreateDto createDto);

    UsersRecordingFullDto update (UsersRecordingUpdateDto updateDto);

    void deleteById(Long id);

    void deleteAll();

}
