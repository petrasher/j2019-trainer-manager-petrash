package by.itstep.trainer.manager.service;

import by.itstep.trainer.manager.dto.trainerDto.TrainerCreateDto;
import by.itstep.trainer.manager.dto.trainerDto.TrainerFullDto;
import by.itstep.trainer.manager.dto.trainerDto.TrainerPreviewDto;
import by.itstep.trainer.manager.dto.trainerDto.TrainerUpdateDto;
import by.itstep.trainer.manager.entity.Trainer;

import java.util.List;

public interface TrainerService {

    List<TrainerPreviewDto> findAll();

    TrainerFullDto findById(Long id);

    TrainerFullDto create (TrainerCreateDto createDto);

    TrainerFullDto update (TrainerUpdateDto updateDto);

    void deleteById(Long id);

    void deleteAll();
}
