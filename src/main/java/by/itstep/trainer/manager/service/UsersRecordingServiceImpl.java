package by.itstep.trainer.manager.service;

import by.itstep.trainer.manager.dto.usersRecordingDto.UsersRecordingCreateDto;
import by.itstep.trainer.manager.dto.usersRecordingDto.UsersRecordingFullDto;
import by.itstep.trainer.manager.dto.usersRecordingDto.UsersRecordingPreviewDto;
import by.itstep.trainer.manager.dto.usersRecordingDto.UsersRecordingUpdateDto;
import by.itstep.trainer.manager.entity.Trainer;
import by.itstep.trainer.manager.mapper.UsersRecordingMapper;
import by.itstep.trainer.manager.repository.UserRecordingRepositoryImpl;
import by.itstep.trainer.manager.repository.UserRecordingRepository;
import by.itstep.trainer.manager.repository.TrainerRepositoryImpl;
import by.itstep.trainer.manager.repository.TrainerRepository;
import by.itstep.trainer.manager.entity.UsersRecording;

import java.util.List;

public class UsersRecordingServiceImpl implements UsersRecordingService {

    private UserRecordingRepository userRecordingRepository = new UserRecordingRepositoryImpl();
    private TrainerRepository trainerRepository = new TrainerRepositoryImpl();

    UsersRecordingMapper mapper = new UsersRecordingMapper();


    @Override
    public List<UsersRecordingPreviewDto> findAll() {
        List<UsersRecording> found = userRecordingRepository.findAll();
        return mapper.mapToDtoList(found);
    }

    @Override
    public UsersRecordingFullDto findById(Long id) {
        UsersRecording found = userRecordingRepository.findById(id);
        return mapper.mapToDto(found);
    }

    @Override
    public UsersRecordingFullDto create(UsersRecordingCreateDto createDto) {
        Trainer trainer = trainerRepository.findById(createDto.getTrainerId());
        UsersRecording toSave = mapper.mapToEntity(createDto,trainer);

        UsersRecording created = userRecordingRepository.create(toSave);

        return mapper.mapToDto(created);
    }

    @Override
    public UsersRecordingFullDto update(UsersRecordingUpdateDto updateDto) {
        UsersRecording toUpdate = mapper.mapToEntity(updateDto);
        UsersRecording existingEntity = userRecordingRepository.findById(updateDto.getId());

        toUpdate.setRecordingTime(toUpdate.getRecordingTime());
        toUpdate.setTrainer(existingEntity.getTrainer());

        UsersRecording updated = userRecordingRepository.update(toUpdate);
        return mapper.mapToDto(updated);
    }

    @Override
    public void deleteById(Long id) {
        userRecordingRepository.deleteById(id);

    }

    @Override
    public void deleteAll() {

    }
}
