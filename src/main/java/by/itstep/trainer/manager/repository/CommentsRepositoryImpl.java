package by.itstep.trainer.manager.repository;

import by.itstep.trainer.manager.entity.Comments;
import by.itstep.trainer.manager.util.EntityManagerUtil;

import javax.persistence.EntityManager;
import java.util.List;

public class CommentsRepositoryImpl implements CommentsRepository {
    @Override
    public List<Comments> findAll() {
        EntityManager em = EntityManagerUtil.getEntityManager();

        List <Comments> foundList = em.createNativeQuery("SELECT * FROM comments", Comments.class)
                .getResultList();

        em.close();
        return foundList;
    }

    @Override
    public Comments findById(Long id) {
        EntityManager em = EntityManagerUtil.getEntityManager();

        Comments foundComments = em.find(Comments.class, id);

        em.close();
        return foundComments;

    }

    @Override
    public Comments create(Comments comments) {
        EntityManager em = EntityManagerUtil.getEntityManager();
        em.getTransaction().begin();

        em.persist(comments);

        em.getTransaction().commit();
        em.close();
        return comments;

    }

    @Override
    public Comments update(Comments comments) {
        EntityManager em = EntityManagerUtil.getEntityManager();
        em.getTransaction().begin();

        em.persist(comments);

        em.getTransaction().commit();
        em.close();
        return comments;

    }

    @Override
    public void deleteById(Long id) {
        EntityManager em = EntityManagerUtil.getEntityManager();
        em.getTransaction().begin();

        Comments foundComments = em.find(Comments.class, id);
        em.remove(foundComments);

        em.getTransaction().commit();
        em.close();

    }

    @Override
    public void deleteAll() {
        EntityManager em = EntityManagerUtil.getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM comments").executeUpdate();

        em.getTransaction().commit();
        em.close();

    }
}
