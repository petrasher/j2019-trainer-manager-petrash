package by.itstep.trainer.manager.repository;

import by.itstep.trainer.manager.entity.Trainer;
import by.itstep.trainer.manager.util.EntityManagerUtil;
import org.hibernate.Hibernate;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import java.util.List;

public class TrainerRepositoryImpl implements TrainerRepository {
    @Override
    public List<Trainer> findAll() {
        EntityManager em = EntityManagerUtil.getEntityManager();

        List <Trainer> foundList = em.createNativeQuery("SELECT * FROM trainer", Trainer.class)
                .getResultList();

        em.close();
        return foundList;
    }

    @Override
    public Trainer findById(Long id)  {
        EntityManager em = EntityManagerUtil.getEntityManager();

        Trainer foundTrainer = em.find(Trainer.class, id);

        if(foundTrainer != null){
            Hibernate.initialize(foundTrainer.getComments());
            Hibernate.initialize(foundTrainer.getUsersRecordings());
        }
        em.close();
        System.out.println("Found collection: " + foundTrainer);
        return foundTrainer;

    }

    @Override
    public Trainer create(Trainer trainer) {
        EntityManager em = EntityManagerUtil.getEntityManager();
        em.getTransaction().begin();

        em.persist(trainer);

        em.getTransaction().commit();
        em.close();
        return trainer;
    }

    @Override
    public Trainer update(Trainer trainer) {
        EntityManager em = EntityManagerUtil.getEntityManager();
        em.getTransaction().begin();

        em.persist(trainer);

        em.getTransaction().commit();
        em.close();
        return trainer;
    }

    @Override
    public void deleteById(Long id) {
        EntityManager em = EntityManagerUtil.getEntityManager();
        em.getTransaction().begin();

        Trainer foundTrainer = em.find(Trainer.class,id);
        em.remove(foundTrainer);

        em.getTransaction().commit();
        em.close();

    }

    @Override
    public void deleteAll() {
        EntityManager em = EntityManagerUtil.getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM trainer").executeUpdate();

        em.getTransaction().commit();
        em.close();

    }
}
