package by.itstep.trainer.manager.repository;

import by.itstep.trainer.manager.entity.UsersRecording;
import by.itstep.trainer.manager.util.EntityManagerUtil;

import javax.persistence.EntityManager;
import java.util.List;

public class UserRecordingRepositoryImpl implements UserRecordingRepository {
    @Override
    public List<UsersRecording> findAll() {
        EntityManager em = EntityManagerUtil.getEntityManager();

        List<UsersRecording> foundList = em.createNativeQuery("SELECT * FROM users_recording", UsersRecording.class)
                .getResultList();

        em.close();
        return foundList;
    }

    @Override
    public UsersRecording findById(Long id) {
        EntityManager em = EntityManagerUtil.getEntityManager();

        UsersRecording foundUserRecording = em.find(UsersRecording.class, id);

        em.close();
        return foundUserRecording;
    }

    @Override
    public UsersRecording create(UsersRecording usersRecording) {
        EntityManager em = EntityManagerUtil.getEntityManager();
        em.getTransaction().begin();

        em.persist(usersRecording);

        em.getTransaction().commit();
        em.close();
        return usersRecording;
    }

    @Override
    public UsersRecording update(UsersRecording usersRecording) {
        EntityManager em = EntityManagerUtil.getEntityManager();
        em.getTransaction().begin();

        em.persist(usersRecording);

        em.getTransaction().commit();
        em.close();
        return usersRecording;
    }

    @Override
    public void deleteById(Long id) {
        EntityManager em = EntityManagerUtil.getEntityManager();

        UsersRecording foundUserRecording = em.find(UsersRecording.class, id);
        em.remove(foundUserRecording);
        em.getTransaction().begin();

        em.getTransaction().commit();
        em.close();


    }

    @Override
    public void deleteAll() {
        EntityManager em = EntityManagerUtil.getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM users_recording").executeUpdate();

        em.getTransaction().commit();
        em.close();

    }
}
