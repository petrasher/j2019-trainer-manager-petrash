package by.itstep.trainer.manager.repository;

import by.itstep.trainer.manager.entity.UsersRecording;

import java.util.List;

public interface UserRecordingRepository {


    List<UsersRecording> findAll();

    UsersRecording findById(Long id);

    UsersRecording create (UsersRecording usersRecording);

    UsersRecording update (UsersRecording usersRecording);

    void deleteById(Long id);

    void deleteAll();
}
