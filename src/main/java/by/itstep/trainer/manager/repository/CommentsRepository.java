package by.itstep.trainer.manager.repository;

import by.itstep.trainer.manager.entity.Comments;
import by.itstep.trainer.manager.entity.Trainer;

import java.util.List;

public interface CommentsRepository {

    List<Comments> findAll();

    Comments findById(Long id);

    Comments create (Comments comments);

    Comments update (Comments comments);

    void deleteById(Long id);

    void deleteAll();
}
