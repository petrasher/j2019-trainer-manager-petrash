package by.itstep.trainer.manager.repository;

import by.itstep.trainer.manager.entity.Admins;
import by.itstep.trainer.manager.util.EntityManagerUtil;

import javax.persistence.EntityManager;
import java.util.List;

public class AdminsRepositoryImpl implements AdminsRepository {
    @Override
    public List<Admins> findAll() {
        EntityManager em = EntityManagerUtil.getEntityManager();

        List <Admins> foundList = em.createNativeQuery("SELECT * FROM admins", Admins.class)
                .getResultList();

        em.close();
        return foundList;

    }

    @Override
    public Admins findById(Long id) {
        EntityManager em = EntityManagerUtil.getEntityManager();

        Admins foundAdmin = em.find(Admins.class, id);

        em.close();
        return foundAdmin;
    }

    @Override
    public Admins create(Admins admins) {
        EntityManager em = EntityManagerUtil.getEntityManager();
        em.getTransaction().begin();

        em.persist(admins);

        em.getTransaction().commit();
        em.close();
        return admins;

    }

    @Override
    public Admins update(Admins admins) {
        EntityManager em = EntityManagerUtil.getEntityManager();
        em.getTransaction().begin();

        em.persist(admins);

        em.getTransaction().commit();
        em.close();
        return admins;
    }

    @Override
    public void deleteById(Long id) {
        EntityManager em = EntityManagerUtil.getEntityManager();
        em.getTransaction().begin();

        Admins foundAdmins = em.find(Admins.class, id);
        em.remove(foundAdmins);

        em.getTransaction().commit();
        em.close();


    }

    @Override
    public void deleteAll() {
        EntityManager em = EntityManagerUtil.getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM admins").executeUpdate();

        em.getTransaction().commit();
        em.close();

    }
}
