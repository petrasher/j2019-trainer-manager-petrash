package by.itstep.trainer.manager.repository;

import by.itstep.trainer.manager.entity.Admins;
import by.itstep.trainer.manager.entity.Comments;

import java.util.List;

public interface AdminsRepository {

    List<Admins> findAll();

    Admins findById(Long id);

    Admins create (Admins admins);

    Admins update (Admins admins);

    void deleteById(Long id);

    void deleteAll();
}
