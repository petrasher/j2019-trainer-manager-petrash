package by.itstep.trainer.manager;

import by.itstep.trainer.manager.entity.Comments;
import by.itstep.trainer.manager.entity.Trainer;
import by.itstep.trainer.manager.repository.CommentsRepository;
import by.itstep.trainer.manager.repository.CommentsRepositoryImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

public class CommentsRepositoryTest {

    private CommentsRepository commentsRepository = new CommentsRepositoryImpl();

    @BeforeEach
    void setUp() {
        commentsRepository.deleteAll();
    }

    @Test
    void findAll() {
        //Given
        Comments comments = Comments.builder()

                .name("name")
                .email("email")
                .build();

        commentsRepository.create(comments);

        //When
        List<Comments> commentsList = commentsRepository.findAll();

        //Then
        Assertions.assertEquals(1, commentsList.size());

    }
    @Test
    void findById(){
        //Given
        Comments comments = Comments.builder()
                .name("name")
                .email("email")
                .build();
        commentsRepository.create(comments);

        //When
        Comments comments1 = commentsRepository.findById(comments.getId());

        //Then
        Assertions.assertEquals(comments,comments1);

    }

    @Test
    void create(){
        //Given
        Comments comments = Comments.builder()
                .name("name")
                .email("email")
                .build();

        //When

        Comments saved = commentsRepository.create(comments);

        //Then

        Assertions.assertNotNull(saved.getId());
    }

    @Test
    void update(){
        //Given
        Comments comments = Comments.builder()
                .name("name")
                .email("email")
                .build();

        //When

        Comments saved = commentsRepository.update(comments);

        //Then

        Assertions.assertNotNull(saved.getId());
    }


    @Test
    void delete(){
        //Given
        Comments comments = Comments.builder()
                .name("Name")
                .email("email")
                .build();
        Comments toDelete = commentsRepository.create(comments);
        //When

        commentsRepository.deleteById(toDelete.getId());
        List<Comments> commentsList = commentsRepository.findAll();

        //Then
        Assertions.assertEquals(commentsList.contains(toDelete),false);

    }
}

