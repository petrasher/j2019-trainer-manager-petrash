package by.itstep.trainer.manager;

import by.itstep.trainer.manager.entity.UsersRecording;
import by.itstep.trainer.manager.repository.UserRecordingRepository;
import by.itstep.trainer.manager.repository.UserRecordingRepositoryImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

public class UserRecordingRepositoryTest {

    private UserRecordingRepository userRecordingRepository = new UserRecordingRepositoryImpl();

    @BeforeEach
    void setUp() {
        userRecordingRepository.deleteAll();
    }

    @Test
    void findAll() {
        //Given
        UsersRecording userRecording = UsersRecording.builder()
                .name("name")
                .lastName("lastname")
                .build();

        userRecordingRepository.create(userRecording);

        //When
        List<UsersRecording> usersRecordingList = userRecordingRepository.findAll();

        //Then
        Assertions.assertEquals(1, usersRecordingList.size());

    }
    @Test
    void findById(){
        //Given
        UsersRecording userRecording = UsersRecording.builder()
                .name("name")
                .lastName("lastname")
                .email("email")
                .build();
        userRecordingRepository.create(userRecording);

        //When
        UsersRecording usersRecording1 = userRecordingRepository.findById(userRecording.getId());

        //Then
        Assertions.assertEquals(userRecording,usersRecording1);

    }

    @Test
    void create(){
        //Given
        UsersRecording userRecording = UsersRecording.builder()
                .lastName("lastname")
                .email("email")
                .build();

        //When

        UsersRecording saved = userRecordingRepository.create(userRecording);

        //Then

        Assertions.assertNotNull(saved.getId());
    }

    @Test
    void update(){
        //Given
        UsersRecording userRecording = UsersRecording.builder()
                .lastName("lastname")
                .email("email")
                .build();

        //When

        UsersRecording saved = userRecordingRepository.update(userRecording);

        //Then

        Assertions.assertNotNull(saved.getId());
    }


    @Test
    void delete(){
        //Given
        UsersRecording userRecording = UsersRecording.builder()
                .lastName("lastname")
                .email("email")
                .build();
        UsersRecording toDelete = userRecordingRepository.create(userRecording);
        //When

        userRecordingRepository.deleteById(toDelete.getId());
        List<UsersRecording> usersRecordingList = userRecordingRepository.findAll();

        //Then
        Assertions.assertEquals(usersRecordingList.contains(toDelete),false);

    }
}

