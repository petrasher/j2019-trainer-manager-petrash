package by.itstep.trainer.manager;

import by.itstep.trainer.manager.entity.Admins;
import by.itstep.trainer.manager.entity.enums.Role;
import by.itstep.trainer.manager.repository.AdminsRepository;
import by.itstep.trainer.manager.repository.AdminsRepositoryImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

public class AdminsRepositoryTest {

    private AdminsRepository adminsRepository = new AdminsRepositoryImpl();

        @BeforeEach
        void setUp() {
            adminsRepository.deleteAll();
        }

        @Test
        void findAll() {
            //Given
            Admins admins = Admins.builder()

                    .name("name")
                    .lastName("lastName")
                    .password("pass")
                    .email("email")
                    .role(Role.ADMIN)
                    .build();

            adminsRepository.create(admins);

            //When
            List<Admins> adminsList = adminsRepository.findAll();

            //Then
            Assertions.assertEquals(1, adminsList.size());

        }
        @Test
        void findById(){
            //Given
            Admins admins = Admins.builder()
                    .name("name")
                    .lastName("lastName")
                    .email("email")
                    .build();
            adminsRepository.create(admins);

            //When
            Admins admins1 = adminsRepository.findById(admins.getId());

            //Then
            Assertions.assertEquals(admins,admins1);

        }

        @Test
        void create(){
            //Given
            Admins admins = Admins.builder()
                    .name("name")
                    .lastName("lastName")
                    .email("email")
                    .build();

            //When

            Admins saved = adminsRepository.create(admins);

            //Then

            Assertions.assertNotNull(saved.getId());
        }

        @Test
        void update(){
            //Given
            Admins admins = Admins.builder()
                    .name("name")
                    .lastName("lastName")
                    .email("email")
                    .build();

            //When

            Admins saved = adminsRepository.update(admins);

            //Then

            Assertions.assertNotNull(saved.getId());
        }


        @Test
        void delete(){
            //Given
            Admins admins = Admins.builder()
                    .name("Name")
                    .lastName("lastName")
                    .email("email")
                    .build();
            Admins toDelete = adminsRepository.create(admins);
            //When

        adminsRepository.deleteById(toDelete.getId());
        List<Admins> adminsList = adminsRepository.findAll();

        //Then
        Assertions.assertEquals(adminsList.contains(toDelete),false);

    }
}
