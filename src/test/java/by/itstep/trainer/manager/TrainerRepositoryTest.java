package by.itstep.trainer.manager;

import by.itstep.trainer.manager.entity.Comments;
import by.itstep.trainer.manager.entity.Trainer;
import by.itstep.trainer.manager.entity.UsersRecording;
import by.itstep.trainer.manager.repository.*;
import org.hibernate.Hibernate;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.Persistence;
import javax.persistence.PersistenceException;
import java.util.List;

public class TrainerRepositoryTest {


    private CommentsRepository commentsRepository = new CommentsRepositoryImpl();
    private UserRecordingRepository userRecordingRepository = new UserRecordingRepositoryImpl();
    private TrainerRepository trainerRepository = new TrainerRepositoryImpl();

    @BeforeEach
    void setUp() {
        trainerRepository.deleteAll();
    }

    @Test
    void findAll() {
        //Given
        Trainer trainer = Trainer.builder()

                .name("name")
                .lastName("lastname")
                .email("email")
                .build();

        trainerRepository.create(trainer);

        //When
        List<Trainer> trainerList = trainerRepository.findAll();

        //Then
        Assertions.assertEquals(1, trainerList.size());

    }

    @Test
    void findById() {
        //Given
        Trainer trainer = Trainer.builder()
                .name("name")
                .email("email")
                .build();
        trainerRepository.create(trainer);

        //When
        Trainer trainer1 = trainerRepository.findById(trainer.getId());

        //Then
        Assertions.assertEquals(trainer, trainer1);

    }

    @Test
    void create() {
        //Given
        Trainer trainer = Trainer.builder()
                .name("name")
                .email("email")
                .build();

        //When

        Trainer saved = trainerRepository.create(trainer);

        //Then

        Assertions.assertNotNull(saved.getId());
    }

    @Test
    void update() {
        //Given
        Trainer trainer = Trainer.builder()
                .name("name")
                .email("email")
                .build();

        //When

        Trainer saved = trainerRepository.update(trainer);

        //Then

        Assertions.assertNotNull(saved.getId());
    }


    @Test
    void delete() {
        //Given
        Trainer trainer = Trainer.builder()
                .name("name")
                .email("email")
                .build();
        Trainer toDelete = trainerRepository.create(trainer);
        //When

        trainerRepository.deleteById(toDelete.getId());
        List<Trainer> trainerList = trainerRepository.findAll();

        //Then
        Assertions.assertEquals(trainerList.contains(toDelete), false);

    }

    @Test
    void findByIdWithCommentsAndRecordings() {

        //Given
        Trainer trainer = Trainer.builder()
                .name("name")
                .lastName("lastName")
                .email("email")
                .build();

        Trainer saved = trainerRepository.create(trainer);

        Comments comments = Comments.builder()
                .name("name")
                .trainer(saved)
                .build();

        commentsRepository.create(comments);

        UsersRecording usersRecordingRecording = UsersRecording.builder()
                .name("name")
                .trainer(saved)
                .build();

        userRecordingRepository.create(usersRecordingRecording);

        //When

        Trainer found = trainerRepository.findById(saved.getId());

        //Then
        Assertions.assertNotNull(found.getId());
        Assertions.assertNotNull(found.getComments());
        Assertions.assertNotNull(found.getUsersRecordings());
        Assertions.assertEquals(1, found.getComments().size());
        Assertions.assertEquals(1, found.getUsersRecordings().size());

    }


    @Test
    void findAllWithoutCommentsAndRecordings() {

        //Given
        Trainer trainer = Trainer.builder()
                .name("name")
                .lastName("lastName")
                .build();

        trainerRepository.create(trainer);

        Comments comments = Comments.builder()
                .name("name")
                .LastName("lastName")
                .build();
        commentsRepository.create(comments);

        UsersRecording usersRecording = UsersRecording.builder()
                .name("name")
                .email("email")
                .build();

        userRecordingRepository.create(usersRecording);

        //When
        List<Trainer> trainersList = trainerRepository.findAll();
        for (Trainer trainers : trainersList) {

            //Then

            Assertions.assertFalse(Hibernate.isInitialized(trainers.getUsersRecordings()));
            Assertions.assertFalse(Hibernate.isInitialized(trainers.getComments()));
        }


    }
}

